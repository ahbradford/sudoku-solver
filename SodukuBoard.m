//
//  SodukuBoard.m
//  Solver
//
//  Created by Adam Bradford on 12/23/13.
//  Copyright (c) 2013 abradford. All rights reserved.
//

#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad
#define fontsize        (IDIOM == IPAD) ? 50.0 : 24.0
#define buttonwidth     (IDIOM == IPAD) ? 88.0 : 44.0
#import "SodukuBoard.h"


@interface SodukuBoard()

@property (nonatomic,strong) NSArray *textLabels;

@end

@implementation SodukuBoard

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        self.layer.borderColor = [UIColor blackColor].CGColor;
        self.layer.borderWidth = 4.0f;
        self.layer.cornerRadius = 6.0f;
        self.clipsToBounds = YES;
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    
    CGFloat cellWidth = self.bounds.size.width /9.0f;
    CGFloat cellHeight = self.bounds.size.height / 9.0f;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetStrokeColorWithColor(context,[UIColor blackColor].CGColor);
    CGContextSetLineWidth(context, 8.0f);
   
    // Drawing code
    
    CGContextSetLineWidth(context, 1.0);
    for(int i = 1; i < 9;i++)
    {
        if(i%3 == 0)
        {
            CGContextSetLineWidth(context, 3);
        }
        else
        {
            CGContextSetLineWidth(context, 1.0);
        }
        
        CGContextMoveToPoint(context, 0, cellHeight*i);
        CGContextAddLineToPoint(context, self.bounds.size.width, cellHeight*i);
         CGContextSetStrokeColorWithColor(context,[UIColor blackColor].CGColor);
        CGContextDrawPath(context, kCGPathStroke);
        
    }
    
    for(int i = 1; i < 9;i++)
    {
        
        if(i%3 == 0)
        {
            CGContextSetLineWidth(context, 3);
        }
        else
        {
            CGContextSetLineWidth(context, 1.0);
        }
        CGContextMoveToPoint(context, cellWidth*i, 0);
        CGContextAddLineToPoint(context, cellWidth*i, self.bounds.size.width);
         CGContextSetStrokeColorWithColor(context,[UIColor blackColor].CGColor);
        CGContextDrawPath(context, kCGPathStroke);
    }
    
}
-(UILabel *)labelForIndex:(int)index
{
    return (UILabel *)self.textLabels[index];
}

-(void)updateLabelFrames
{
    CGFloat cellWidth = self.bounds.size.width /9.0f;
    CGFloat cellHeight = self.bounds.size.height / 9.0f;
    if(!self.textLabels)
    {
        NSMutableArray *labels = [[NSMutableArray alloc]initWithCapacity:81];
        for(int i = 0;i<81;i++)
        {
            int column = i%9;
            int row = i/9;
            
            UILabel *label = [[UILabel alloc]init];
            label.frame = CGRectMake(column * cellWidth+2, row*cellHeight+2, cellWidth-4, cellHeight-4);
            label.textAlignment = NSTextAlignmentCenter;
            label.backgroundColor = [UIColor whiteColor];
            label.opaque = YES;
            [labels addObject:label];
            [self addSubview:label];
            label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:fontsize];
        }
        self.textLabels = labels;
    }
}


@end
