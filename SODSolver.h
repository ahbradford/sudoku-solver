//
//  SODSolver.h
//  Solver
//
//  Created by Adam Bradford on 12/23/13.
//  Copyright (c) 2013 abradford. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SODSolverDelegate <NSObject>

-(int[81])solutionFound;
-(void)noSolutionFound;


@end

@interface SODSolver : NSObject

@property (weak,nonatomic) id<SODSolverDelegate> delegate;

-(BOOL)findSolutionForArray:(int[81])input solutionSpace:(int[81])solutionSpace;
@end
