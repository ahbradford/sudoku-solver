//
//  SODSolver.m
//  Solver
//
//  Created by Adam Bradford on 12/23/13.
//  Copyright (c) 2013 abradford. All rights reserved.
//

#import "SODSolver.h"

@interface SODSolver()
{
    int array[81];
    int solutionSpace[81];
    int count;
    
    
}
@property (nonatomic)int solutionsFound;
@end
@implementation SODSolver


-(BOOL)findSolutionForArray:(int[81])input solutionSpace:(int[81])givenSolutionSpace
{
    self.solutionsFound = 0;
    
    for(int i = 0; i < 81; i++)
    {
        solutionSpace[i] = input[i];
        array[i] = input[i];
    }
    
    count = 0;
    
    BOOL result = [self exploreSolutionForIndex:0];
    
    for(int i = 0; i < 81; i++)
    {
        givenSolutionSpace[i] = solutionSpace[i];
    }
    
    NSLog(@"%d",count);
    return result;
    
}

-(BOOL)exploreSolutionForIndex:(int)index
{
    BOOL result = NO;
    if(index == 81)
    {
        return YES;
        self.solutionsFound++;
    }
    
    if(array[index] > 0)
    {
      return [self exploreSolutionForIndex:index+1];
    }
    
    else
    {
        
        for(int i = 1; i <= 9; i ++)
        {
            if([self testNumber:i forIndex:index])
            {   solutionSpace[index] = i;
                result = [self exploreSolutionForIndex:index+1];
                if(result == YES)return result;
                else solutionSpace[index] = array[index];
               
            }
        }
    }
    return result;
}

-(BOOL)testNumber:(int)number forIndex:(int)index
{
 
    
    int column = (index %9 );
    int row = (index /9) ;
    
    if(![self number:number isValidForColumn:column])return NO;
    if(![self number:number isValidForRow:row])return NO;
    if(![self number:number isValidForSquare:[self squareNumberForIndex:index]])return NO;
    
    return YES;
}

-(BOOL)number:(int)number isValidForRow:(int)row
{
    int startIndex = row *9;
    for(int i = startIndex; i < startIndex +9;i++)
    {
        if(solutionSpace[i] == number)
            return NO;
    }
    
    return YES;
}

-(BOOL)number:(int)number isValidForColumn:(int)column
{
   
    for(int i = 0; i < 81 +9;i+=9)
    {
        int index = i + column;
        int numToTest = solutionSpace[ index];
        if(numToTest == number)
            return NO;
    }
    
    return YES;
}
-(BOOL)number:(int)number isValidForSquare:(int)square
{
    int squareRow = square / 3;
    int squareColumn = square % 3;
    
    int actualRow = squareRow * 3;
    int actualColumn = squareColumn *3;
    
    int startIndex = actualRow * 9 + actualColumn;
    
    for(int i = 0; i < 3;i++)
    {
        
        for(int j = 0; j < 3; j++)
        {
            
            int numToTest = solutionSpace[startIndex + j];
            if(numToTest == number)return NO;
        }
        startIndex += 9;
    }
    return YES;
}

-(int)squareNumberForIndex:(int)index
{
    int column = index %9 /3;
    int row = index /9 /3;
    
    int result = row *3  +column;
    return result;
    
}

@end
