//
//  SodukuBoard.h
//  Solver
//
//  Created by Adam Bradford on 12/23/13.
//  Copyright (c) 2013 abradford. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SodukuBoard : UIView
-(void)updateLabelFrames;
-(UILabel *)labelForIndex:(int)index;
@end
