//
//  SODNumberPad.h
//  Solver
//
//  Created by Adam Bradford on 12/23/13.
//  Copyright (c) 2013 abradford. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SODNumberPadDelegate <NSObject>

-(void)numberPressed:(int)number;
-(BOOL)numberIsValid:(int)number;

@end

@interface SODNumberPad : UIView

@property (weak,nonatomic) id<SODNumberPadDelegate> delegate;

-(id)initWithFrame:(CGRect)frame  delegate:(id<SODNumberPadDelegate>)delegate;

@end
