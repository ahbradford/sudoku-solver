//
//  SODNumberPad.m
//  Solver
//
//  Created by Adam Bradford on 12/23/13.
//  Copyright (c) 2013 abradford. All rights reserved.
//

#import "SODNumberPad.h"

#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad
#define fontsize        ((IDIOM == IPAD) ? 50.0 : 24.0)
#define buttonwidth     ((IDIOM == IPAD) ? 88.0 : 44.0)
@interface SODNumberPad()

 @property (nonatomic,strong)   NSArray *numbersArray;
@property (nonatomic,weak) UILabel *bottomlabel;


@end

@implementation SODNumberPad

- (id)initWithFrame:(CGRect)frame delegate:(id<SODNumberPadDelegate>)delegate;
{
    self = [super initWithFrame:frame];
    if (self) {
        _delegate = delegate;
        [self animateViews];
    }
    return self;
}

-(void)animateViews
{
    NSMutableArray *numbersArray = [[NSMutableArray alloc]initWithCapacity:10];
    CGFloat width = self.bounds.size.width /3.0f;
    CGFloat height = self.bounds.size.height /3.0f;
    
    UIColor *backgroundColor = [UIColor colorWithRed:.1 green:.1 blue:.1 alpha:.9];
    
    for(int i = 0; i < 9; i++)
    {
        UIView *numberView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, width, height)];
        
        numberView.layer.cornerRadius = 4;
        
        UILabel *numberlabel = [[UILabel alloc]initWithFrame:CGRectMake(2, 2, width-4, height-4)];
        numberlabel.backgroundColor = backgroundColor;
        numberlabel.text = [NSString stringWithFormat:@"%d",i+1];
        numberlabel.textAlignment = NSTextAlignmentCenter;
        numberlabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:fontsize];
        numberlabel.layer.cornerRadius = 8.0f;
        
        numberView.center = CGPointMake(self.bounds.size.width/2.0f, self.bounds.size.height/2.0f);
        [numberView addSubview:numberlabel];
        
        if(![_delegate numberIsValid:i+1])
        {
            numberlabel.textColor = [UIColor redColor];
        }
        else
        {
            numberlabel.textColor = [UIColor whiteColor];
        }
        
        numberView.alpha = 0;
        
        [numbersArray addObject:numberView];
        self.numbersArray = numbersArray;
        [self addSubview: numberView];
    }
    
    for(int i = 0; i < 9; i++)
    {
        
                [UIView animateWithDuration:.15f animations:^{
                    UIView *view = numbersArray[i];
                    
                    view.alpha = 1;
                    CGFloat originX = width *(i%3);
                    CGFloat originY = height *(i/3);
                    view.frame = (CGRect){CGPointMake(originX, originY),view.frame.size};
                
                    
                }];
        
    }
    
    CGRect oldframe = self.frame;
    oldframe.size.height += buttonwidth;
    self.frame = oldframe;
    
    UILabel *bottomLabel = [[UILabel alloc]initWithFrame:CGRectMake(2, self.bounds.size.height- (buttonwidth) +2, self.bounds.size.width -4, buttonwidth -4) ];
    bottomLabel.text = @"Remove";
    bottomLabel.textColor = [UIColor whiteColor];
    bottomLabel.backgroundColor = backgroundColor;
    bottomLabel.textAlignment   = NSTextAlignmentCenter;
    bottomLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:fontsize];
    bottomLabel.layer.cornerRadius = 8.0f;
    bottomLabel.transform = CGAffineTransformMakeScale(.1, .1);
    [self addSubview:bottomLabel];
    
    
    
    [UIView animateWithDuration:.15f animations:^{
        
        bottomLabel.transform = CGAffineTransformMakeScale(1.0, 1.0);
    }];
    self.bottomlabel = bottomLabel;
    UITapGestureRecognizer *tapGr = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(numberPressed:)];
    [self addGestureRecognizer:tapGr];
    
}

-(void)numberPressed:(UITapGestureRecognizer *)tapGr
{
    CGPoint location = [tapGr locationInView:self];
    
    int row = (int)location.y / (int)buttonwidth;
    int column = (int)location.x / (int) buttonwidth;
    
    int numberPressed = row *3 + column +1;
    
    if(row == 3)
        numberPressed = 0;
    
    if([_delegate numberIsValid:numberPressed] || numberPressed == 0)
    {
        NSLog(@"%d",numberPressed);
        [_delegate numberPressed:numberPressed];
        [self removeFromSuperview];
    }
}

-(void)dispatchAfter:(CGFloat)time block:(void(^)(void))someBlock
{
    double delayInSeconds = time;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        if(someBlock)someBlock();
    });
}

-(void)removeFromSuperview
{
    [UIView animateWithDuration:.15f animations:^
     {
         CGPoint center = CGPointMake(self.bounds.size.width /2.0f, self.bounds.size.height/2.0f);

         for(UIView *view in self.numbersArray)
         {
             view.center =   center;
         }
         
         self.bottomlabel.transform = CGAffineTransformMakeScale(.1, .1);
         self.bottomlabel.center = center;
         
     }completion:^(BOOL finished){
         [super removeFromSuperview];
     }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
