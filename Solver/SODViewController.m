//
//  SODViewController.m
//  Solver
//
//  Created by Adam Bradford on 12/23/13.
//  Copyright (c) 2013 abradford. All rights reserved.
//

#import "SODViewController.h"
#import "SodukuBoard.h"
#import "SODNumberPad.h"
#import "SODSolver.h"

#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad
#define fontsize        (IDIOM == IPAD) ? 30.0 : 24.0
#define buttonwidth     (IDIOM == IPAD) ? 88.0 : 44.0



@interface SODViewController ()<SODNumberPadDelegate>


{
    int array[81];
    int testArray[81];
}
@property (weak, nonatomic) IBOutlet SodukuBoard *board;
@property (weak, nonatomic) SODNumberPad *currentNumberPad;
@property (weak,nonatomic) UIView *blockView;
@property (nonatomic) CGPoint currentEditingCell;
@property (strong,nonatomic) SODSolver *solver;

@property (nonatomic) BOOL revealOneMode;
@property (nonatomic) BOOL solutionObtained;
@property (weak, nonatomic) IBOutlet UIButton *revealOneButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation SODViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    for(int i = 0; i < 81; i++)
    {
        testArray[i]=0;
        array[i]=0;
    }
    
    UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
    [self.board addGestureRecognizer:tapGR];
    
    
    
	// Do any additional setup after loading the view, typically from a nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.board updateLabelFrames];
    [self copySoltionToLabels:array];
    
    
}

-(void)viewDidAppear:(BOOL)animated
{
    UIDeviceOrientation orientation =  [[UIDevice currentDevice]orientation];
    if(UIDeviceOrientationIsLandscape(orientation))
    {
        self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:34];
    }
    else
    {
        self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:100];
        
    }
}

-(void)copySoltionToLabels:(int *)solution
{
    for(int i = 0; i < 81;i++)
    {
        array[i] = solution[i];
        int valueForCell = solution[i];
        UILabel *label = [self.board labelForIndex:i];
        if(valueForCell > 0)
        {
            
            label.text = [NSString stringWithFormat:@"%d",valueForCell];
        }
        else
        {
            label.text = @"";
        }
    }
}

-(void)handleTap:(UITapGestureRecognizer *)tap
{
    //get rid of all the crap
    
    
    
    CGPoint location = [tap locationInView:self.board];
    
    //set up numberpad
    CGFloat cellHeight = (self.board.bounds.size.height /9);
    CGFloat cellWidth = (self.board.bounds.size.width / 9);
    int row = (int)location.y / (int)cellHeight;
    int column = (int)location.x / (int)cellWidth;
    
    self.currentEditingCell = CGPointMake(column, row);
    
    if(self.revealOneMode)
    {
        [self revealOnePressed:self.revealOneButton];
        int index = [self indexForPoint:self.currentEditingCell];
        [self revealIndex:index];
        
    }
    else
    {
        self.solutionObtained = NO;
        
        //add a blocker view
        UIView *blockView = [[UIView alloc]initWithFrame:self.view.bounds];
        UIGestureRecognizer *tapGr = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(blockPressed:)];
        [blockView addGestureRecognizer:tapGr];
        [self.board addSubview:blockView];
        self.blockView = blockView;
        
        SODNumberPad *numberPad = [[SODNumberPad alloc]initWithFrame:CGRectMake(0, 0, (buttonwidth)*3, (buttonwidth)*3) delegate:self];
        
        [self.view addSubview:numberPad];
        
        self.currentNumberPad = numberPad;
        
        
        
        CGPoint center = CGPointMake(column * cellWidth + (cellWidth/2.0f), row * cellHeight + (cellHeight /2) +((buttonwidth)/2) );
        center = [self.board convertPoint:center toView:self.view];
        numberPad.center = center;
        
        
            //check and adjust the numberpad so it is on the screen on an iphone.
            if(numberPad.frame.origin.x < 0)
            {
                CGRect frame    = numberPad.frame;
                frame.origin.x  = 0.0f;
                numberPad.frame = frame;
            }
            else if(numberPad.frame.origin.x + numberPad.bounds.size.width > self.view.bounds.size.width)
            {
                CGRect frame    = numberPad.frame;
                frame.origin.x  = self.view.bounds.size.width - numberPad.bounds.size.width ;
                numberPad.frame = frame;
            }
        
    
      
        
        
    }
    
}

-(void)calculateSolution
{
    self.solver = [[SODSolver alloc]init];
    
    for(int i = 0; i < 81;i++)testArray[i]=0;
    
    [self.solver findSolutionForArray:array solutionSpace:testArray];
    self.solutionObtained = YES;
}

-(void)revealIndex:(int)index
{
    int result = testArray[index];
    array[index] = testArray[index];
    
    UILabel *label = [self.board labelForIndex:index];
    label.text = [NSString stringWithFormat:@"%d",result];
    
}

- (IBAction)revealAllPressed:(id)sender
{
    if(!self.currentNumberPad)
    {
        if(self.revealOneMode)
            [self revealOnePressed:self.revealOneButton];
        if(!self.solutionObtained)
            [self calculateSolution];
        [self copySoltionToLabels:testArray];
    }
    
}
- (IBAction)revealOnePressed:(id)sender
{if(!self.currentNumberPad)
{
    if(!self.solutionObtained)
        [self calculateSolution];
    
    if(self.revealOneMode)
    {
        
        self.revealOneMode = NO;
        self.revealOneButton.backgroundColor = [UIColor clearColor];
        [self.revealOneButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];;
        
    }
    else
    {
        self.revealOneMode = YES;
        self.revealOneButton.backgroundColor = [UIColor darkGrayColor];
                [self.revealOneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];;
        self.revealOneButton.layer.cornerRadius = 6.0f;
        
    }
}
}
- (IBAction)clearButtonPressed:(id)sender
{
    if(!self.currentNumberPad)
    {
        if(self.revealOneMode)
            [self revealOnePressed:self.revealOneButton];
        
        
        self.solutionObtained = NO;
        self.revealOneMode = NO;
        for(int i = 0; i < 81; i++)
        {
            array[i] = 0;
            testArray[i] = 0;
        }
        [self copySoltionToLabels:array];
    }
}

-(int)indexForPoint:(CGPoint)point
{
    int index = point.x + point.y * 9;
    NSLog(@"%d",index);
    return index;
    
}

-(void)numberPressed:(int)number
{
    
    int index = [self indexForPoint:self.currentEditingCell];
    array[index] = number;
    
    UILabel *label = [self.board labelForIndex:index];
    
    if(number > 0)
    {
        label.text = [NSString stringWithFormat:@"%d",number];
    }
    else
    {
        label.text = @"";
    }
    
    
    [self.blockView removeFromSuperview];
    
}

-(void)blockPressed:(UITapGestureRecognizer *)tapGr
{
    [self.currentNumberPad removeFromSuperview];
    [tapGr.view removeFromSuperview];
    
    
}

-(BOOL)numberIsValid:(int)number
{
    int index = [self indexForPoint:self.currentEditingCell];
    BOOL isValid = [self testNumber:number forIndex:index];
    return isValid;
}

#pragma mark Test If Number Is Valid At Index
-(BOOL)testNumber:(int)number forIndex:(int)index
{
    
    
    int column = (index %9 );
    int row = (index /9) ;
    
    if(![self number:number isValidForColumn:column])return NO;
    if(![self number:number isValidForRow:row])return NO;
    if(![self number:number isValidForSquare:[self squareNumberForIndex:index]])return NO;
    
    return YES;
}

-(BOOL)number:(int)number isValidForRow:(int)row
{
    int startIndex = row *9;
    for(int i = startIndex; i < startIndex +9;i++)
    {
        if(array[i] == number)
            return NO;
    }
    
    return YES;
}

-(BOOL)number:(int)number isValidForColumn:(int)column
{
    
    for(int i = 0; i < 81 +9;i+=9)
    {
        int index = i + column;
        int numToTest = array[index];
        if(numToTest == number)
            return NO;
    }
    
    return YES;
}
-(BOOL)number:(int)number isValidForSquare:(int)square
{
    int squareRow = square / 3;
    int squareColumn = square % 3;
    
    int actualRow = squareRow * 3;
    int actualColumn = squareColumn *3;
    
    int startIndex = actualRow * 9 + actualColumn;
    
    for(int i = 0; i < 3;i++)
    {
        
        for(int j = 0; j < 3; j++)
        {
            
            int numToTest = array[startIndex + j];
            if(numToTest == number)return NO;
        }
        startIndex += 9;
    }
    return YES;
}

-(int)squareNumberForIndex:(int)index
{
    int column = index %9 /3;
    int row = index /9 /3;
    
    int result = row *3  +column;
    return result;
    
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if(UIDeviceOrientationIsLandscape(toInterfaceOrientation))
    {
        self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:34];
    }
    else
    {
        self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:100];

    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
